#ifndef POLYGONFIGURE_H
#define POLYGONFIGURE_H

#include <QPainter>
#include <QGraphicsPolygonItem>
#include <QGraphicsItem>
#include <QDebug>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsSceneHoverEvent>
#include <QList>
#include <QUuid>
#include <QVariant>
#include <QString>

class PolygonFigure : public QGraphicsPolygonItem
{    
public:
    PolygonFigure(QList<QPointF> point);

    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget); 
};

#endif // POLYGONFIGURE_H
