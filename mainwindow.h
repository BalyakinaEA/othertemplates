#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QDebug>
#include <QSettings>
#include <QDir>
#include <QCoreApplication>
#include <QThread>
#include <QMutex>
#include <QMutexLocker>
#include <QUuid>
#include <QHash>
#include <QMap>
#include <QUuid>
#include <QPixmap>
#include <QFont>
#include <QFontDatabase>
#include <QtSql/QSqlDatabase>
#include <QSqlQuery>
#include <QList>

#include "quazip/quazip.h"
#include "quazip/quazipfile.h"

#include "CustomSocket.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    CustomSocket* socketObject;
    QThread *threadSocket;

private slots:
    void creatThreds();
    void checkSavedFile();
    void unzipSavedFile();
    void copyPath(const QString & oldPath, const QString & newPath);

public slots:
    void receiveSocketFinished();
};

#endif // MAINWINDOW_H
