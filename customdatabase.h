#ifndef CustomDataBase_H
#define CustomDataBase_H

#include <QObject>
#include <QSql>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <QFile>
#include <QDate>
#include <QUuid>
#include <QDebug>
#include <QPoint>
#include <QFile>
#include <QList>
#include <QMap>
#include <QMutex>
#include <QMutexLocker>
#include <QSettings>

#define DATABASE_HOSTNAME   "DataBase"
#define DATABASE_NAME       "DataBaseName.db"

#define TABLE                   "SomeTableName"
#define TABLE_UID_OBJECT        "UidObject"

class CustomDataBase : public QObject
{
    Q_OBJECT
public:
    explicit CustomDataBase(QObject *parent = 0);
    ~CustomDataBase();

    bool connectToDataBase();
    bool openDataBase();
    bool restoreDataBase();
    void closeDataBase();
    bool checkExistObjectTable(QString uidObject);
    bool createTable();
    bool createNewJournalTable(QString nameTable);

    bool inserIntoTable(quint8 section, QString nameTable, const QVariantList &data);
    void deleteFromTable(quint8 section, QString nameTable, QString uidObject);

    bool updateDataToMainTable(QList<QString> newList);
    void selectInfoFromTable(QString uidObject);
    void clearAllDataTable(QString id);

private:
    QSqlDatabase db;
    QString homePath;
    QMap<quint8, QString> sectionsMap;
    QSettings *settings;

signals:
    void sendDataFromDB(quint8 section, QVariantList listQueryValues);
    void sendDBFinished();

public slots:
     void receiveNeedCloseDB();
     void receiveAddDataToDBSection(quint8 section, QString time, ObjectK *currentObject);
     void receiveDeleteDataFromDBSection(quint8 section, QString uidObject);
};

#endif // CustomDataBase_H
