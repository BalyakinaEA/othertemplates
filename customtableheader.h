#ifndef CustomTableHeader_H
#define CustomTableHeader_H

#include <QObject>
#include <QHeaderView>
#include <QTableView>
#include <QDebug>
#include <QComboBox>
#include <QCheckBox>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QRect>
#include <QMouseEvent>
#include <QList>
#include <QVector>
#include <QModelIndex>
#include <QVariant>
#include <QPushButton>
#include <QLineEdit>
#include <QIcon>

class CustomTableHeader :  public QHeaderView
{
    Q_OBJECT
public:
    explicit CustomTableHeader(Qt::Orientation orientation = Qt::Horizontal, QWidget *parent = 0);
    enum Order {
        AscendingOrder,
        DescendingOrder,
        Unsorted
    };

    void mouseReleaseEvent(QMouseEvent* e);
    void mousePressEvent(QMouseEvent* e);

    bool eventFilter(QObject *o, QEvent *e);   
    void createFilterButton();


private slots:
    void getNameToSearch();
    void getFactoryNumber();
    void setSortingOrder(int column);
private:
    Order order;   
    QPushButton *filterBtn;
    QLineEdit *typeEditor;
    QLineEdit *factoryEditor;

signals:    
    void sendPressOpenFilterBtn(QPoint pos);
    void sendSearchDeviceName(QString name);
    void sendSearchFactoryNumber(QString factory);
    void sendResetSorting();

public slots:
};

#endif // CustomTableHeader_H
