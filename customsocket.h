#ifndef CustomSocket_H
#define CustomSocket_H

#include <QObject>
#include <QDebug>
#include <QUdpSocket>
#include <QTimer>
#include <QSettings>
#include <QFile>
#include <QCryptographicHash>
#include <QDir>
#include <QMutex>
#include <QMutexLocker>
#include <QWaitCondition>
#include <QStandardItem>
#include <QQueue>
#include <QList>
#include <QUuid>
#include <QVector>
#include <QDateTime>

#include "journalprocesses.h"

class CustomSocket : public QObject
{
    Q_OBJECT
public:
    explicit CustomSocket(QObject *parent = nullptr);
    void createSWData();

    enum Actions{
        NOT_TO_DO,
        CHECK_CONNECTION,
        LOAD_FILE,
        GIVE_INFO,
    };

private slots:
    void timerActions();
    void sendDatagram();
    void takeDatagram();

private:
    QUdpSocket *socket;

    QSettings *settings;

    QTimer *timerOfAction;
    int choiceOfAction;

    quint8 repeatedRequest;
    QByteArray datagram;

    quint32 savedBlocs;
    QFile savedFile;

    quint32 savedPart;

    quint32 currentPack;
    quint32 numberProject;

    JournalProcesses *journalProcess;
    QFile *logFile;

    QMap<quint32, ObjectK *> *objectsMap;

signals:
    void sendSocketFinished();
    void sendNotAvaliable();
    void sendLoadingFinished();
    void sendErrorLoadingFile();
    void sendAddNewJournalRecords();
    void sendNeedDrawJournal(QList<QStandardItem* > list);
    void sendConnectionAccept();

public slots:
    void receiveConnectToCurrentHost(QString ip);
    void receiveSocketStartTimer();
    void receiveSocketStopTimer();
    void receiveNeedCloseSocket();

};

#endif // CustomSocket_H
