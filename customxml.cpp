#include "CustomXML.h"

CustomXML::CustomXML(QString fileName)
{
    fileXml.setFileName(fileName);   
    openedXmlVersion = 0;  
}

void CustomXML::setFileNameParseXML(QString fileName)
{
    fileXml.setFileName(fileName);
}
//------------------PARSE TYpe LIST-------------------------
QHash<unsigned int, TypeDevice *> CustomXML::parseTypeListMain()
{
    QHash<unsigned int, TypeDevice *> tempHashDevice;
    if(fileXml.open(QFile::ReadOnly | QFile::Text))
    {
        readerXml.setDevice(&fileXml);
        while(!readerXml.atEnd())
        {
            readerXml.readNext();
            if(readerXml.isStartElement() && readerXml.name() == "XmlVersion")
            {
                openedXmlVersion =  readerXml.readElementText().toUInt();
            }
            if(readerXml.isStartElement() && readerXml.name() == "TypeDescription")
            {
                while(!((readerXml.isEndElement()) && (readerXml.name() == "TypeDescription")))
                {
                    readerXml.readNext();
                    if(readerXml.isStartElement() && (readerXml.name() == "Type"))
                    {
                        TypeDevice *typeDevices = new TypeDevice();
                        parseType(typeDevices);                        
                        tempHashDevice.insert(typeDevices->getTypeNumber(),typeDevices);
                    }
                }
            }
        }
        fileXml.close();
        return tempHashDevice;
    }
    else
    {
        QHash<unsigned int, TypeDevice *> emptyHashDevice;
        return emptyHashDevice;
        fileXml.close();
    }
}

void CustomXML::parseType(TypeDevice *typeDevices)
{
    while(!readerXml.atEnd())
    {
        readerXml.readNext();
        if(readerXml.isStartElement() && readerXml.name() == "Name")
        {
            typeDevices->setName(readerXml.readElementText());
        }    
        if(readerXml.isStartElement() && readerXml.name() == "IconFile")
        {
            typeDevices->setIconPath(readerXml.readElementText());
        }
        if(readerXml.isStartElement() && readerXml.name() == "Type")
        {
            int tempType = readerXml.readElementText().toInt();           
            /* some code */
            typeDevices->setTypeNumber(tempType);

        }
        if(readerXml.isStartElement() && readerXml.name() == "Group")
        {
            typeDevices->setGroupType(readerXml.readElementText().toInt());
        }
        if(readerXml.isStartElement() && readerXml.name() == "Parameters")
        {
            while(!((readerXml.isEndElement()) && (readerXml.name() == "Parameters")))
            {
                readerXml.readNext();
                if(readerXml.isStartElement() && (readerXml.name() == "Parameter"))
                {                    
                    parseParametr(typeDevices);                   
                }
            }
        }
        if(readerXml.isStartElement() && readerXml.name() == "Buttons")
        {
            while(!((readerXml.isEndElement()) && (readerXml.name() == "Buttons")))
            {
                readerXml.readNext();
                if(readerXml.isStartElement() && (readerXml.name() == "Button"))
                {
                    parseButtons(typeDevices);
                }
            }
        }   
        if(readerXml.isStartElement() && readerXml.name() == "Measurements")
        {
            while(!((readerXml.isEndElement()) && (readerXml.name() == "Measurements")))
            {
                readerXml.readNext();
                if(readerXml.isStartElement() && (readerXml.name() == "Measurement"))
                {
                    typeDevices->measureList.append(readerXml.readElementText());
                }
            }
        }
        if(readerXml.isStartElement() && readerXml.name() == "MeasurementFormula")
        {
            typeDevices->setMeasureFormula(readerXml.readElementText());
        } 
        if(readerXml.isEndElement() && readerXml.name() == "Type")
        {
            readerXml.readNext();
            break;
        }
    }
}

void CustomXML::parseParametr(TypeDevice *typeDevices)
{
    TypeDevice::Parameter paramsStruct;
    while(!readerXml.atEnd())
    {
        readerXml.readNext();
        if(readerXml.isStartElement() && readerXml.name() == "Name")
        {
            paramsStruct.name = readerXml.readElementText();
        }
        if(readerXml.isStartElement() && readerXml.name() == "Address")
        {
            paramsStruct.address = readerXml.readElementText().toUInt();
        }
        if(readerXml.isStartElement() && readerXml.name() == "Value")
        {
            paramsStruct.value = readerXml.readElementText().toInt();
        }
        if(readerXml.isStartElement() && readerXml.name() == "MinValue")
        {
            paramsStruct.minValue = readerXml.readElementText().toInt();
        }
        if(readerXml.isStartElement() && readerXml.name() == "MaxValue")
        {
            paramsStruct.maxValue = readerXml.readElementText().toInt();
        }
        if(readerXml.isStartElement() && readerXml.name() == "EditView")
        {
            paramsStruct.editView = readerXml.readElementText().toInt();
        }  
        if(readerXml.isEndElement() && readerXml.name() == "Parameter")
        {
            typeDevices->parametrsVec.append(paramsStruct);
            readerXml.readNext();
            break;
        }
    }
}

void CustomXML::parseButtons(TypeDevice *typeDevices)
{
    TypeDevice::Buttons buttonsStruct;
    while(!readerXml.atEnd())
    {
        readerXml.readNext();
        if(readerXml.isStartElement() && readerXml.name() == "Name")
        {
            buttonsStruct.BtnName = readerXml.readElementText();
        }
        if(readerXml.isStartElement() && readerXml.name() == "Command")
        {
            QStringList strList = readerXml.readElementText().split(" ", QString::SkipEmptyParts);
            for(int i = 0; i < strList.size(); i++)
            {
                buttonsStruct.BtnCommand << strList.at(i).toInt();
            }
        }
        if(readerXml.isStartElement() && readerXml.name() == "Row")
        {
            buttonsStruct.BtnRow = readerXml.readElementText().toInt();
        }
        if(readerXml.isStartElement() && readerXml.name() == "Column")
        {
            buttonsStruct.BtnColumn = readerXml.readElementText().toInt();
        }
        if(readerXml.isEndElement() && readerXml.name() == "Button")
        {
            typeDevices->buttonsVec.append(buttonsStruct);
            readerXml.readNext();
            break;
        }
    }
}

