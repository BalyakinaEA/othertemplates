#include "CustomTableDelegate.h"

CustomTableDelegate::CustomTableDelegate(QSettings *newSettings, SettingsProcesses *newSettingsProcess)
{
    settings = newSettings;
    settingsProcess = newSettingsProcess;
}

void CustomTableDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItem opt = option;
    initStyleOption(&opt, index);

    QRect rect = option.rect;

    QBrush br = index.model()->data(index, Qt::BackgroundRole).value<QBrush>();
    painter->fillRect(option.rect, br.color());

    bool is_selected = option.state & (QStyle::State_Selected | QStyle::State_HasFocus);

    painter->save();
    if(is_selected)
    {
        qreal redF = br.color().redF();
        qreal greenF =   br.color().greenF();
        qreal blueF = br.color().blueF();
        QColor prevColor(redF*255, greenF*255, blueF*255);

        qreal redNew = redF*255/2;
        qreal greenNew = greenF*255/2;
        qreal blueNew = blueF*255/2;

        QColor newColor(redNew,greenNew, blueNew);

        painter->setPen(Qt::black);
        bool needRepaint = true;
        PSD::ColorScheme scheme = (PSD::ColorScheme)settings->value(settingsProcess->getColorSchemeDirectory()).toInt();
        switch(scheme)
        {
        case PSD::Color:
            if(prevColor == Qt::white)
            {
                needRepaint = false;
                QApplication::style()->drawPrimitive(QStyle::PE_PanelItemViewItem, &opt, painter, opt.widget);
            }
            break;
        case PSD::Gray:
            if(prevColor == Qt::white)
                newColor = QColor(Qt::gray);
            break;
        default:
            if(prevColor == Qt::white)
                newColor = QColor(Qt::gray);
            break;
        }

        if(needRepaint)
        {
            painter->setPen(Qt::white);
            br.setColor(QColor(newColor));
            painter->fillRect(option.rect, br.color());
        }
    }

    QString contactURI = index.model()->data(index, Qt::DisplayRole).toString();
    painter->drawText(rect.adjusted(5,0,0,0) , Qt::AlignVCenter, contactURI);

    QIcon icon = index.model()->data(index, Qt::DecorationRole).value<QIcon>();

    QPixmap iconPixmap = icon.pixmap(QSize(16,16));
    painter->drawPixmap(rect.x()+3, rect.y() + 10, iconPixmap);

    painter->restore();
}


