#ifndef CustomTableDelegate_H
#define CustomTableDelegate_H
#include <QObject>
#include <QStyledItemDelegate>
#include <QItemDelegate>
#include <QComboBox>
#include <QDebug>
#include <QLabel>
#include <QPainter>
#include <QStaticText>
#include <QApplication>
#include <QCoreApplication>
#include <QTreeView>
#include <QEvent>
#include <QMouseEvent>
#include <QPoint>
#include <QCursor>
#include <QLineEdit>
#include <QRegExp>
#include <QValidator>
#include <QStyle>
#include <QVariant>
#include <QMap>
#include <QAbstractItemModel>
#include <QRgb>
#include <QSettings>
#include <QPixmap>

#include "settingsprocesses.h"


class CustomTableDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    CustomTableDelegate(QSettings *newSettings, SettingsProcesses *newSettingsProcess);
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
    QSettings *settings;
    SettingsProcesses *settingsProcess;
};

#endif // CustomTableDelegate_H
