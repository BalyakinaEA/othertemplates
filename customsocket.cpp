#include "CustomSocket.h"

CustomSocket::CustomSocket(QObject *parent) : QObject(parent)
{    
    choiceOfAction = NOT_TO_DO;
    repeatedRequest = 0;
    datagram.clear();
    /* some code */
}

void CustomSocket::createSWData()
{
    /* some code */
    socket = new QUdpSocket(this);
    connect(socket, &QUdpSocket::readyRead, this, &CustomSocket::takeDatagram);

    timerOfAction = new QTimer(this);
    timerOfAction->setInterval(1000);
    connect(timerOfAction, &QTimer::timeout, this, &CustomSocket::timerActions);
}

void CustomSocket::timerActions()
{   
    switch(choiceOfAction)
    {
    case NOT_TO_DO:{
        break;}
    case CHECK_CONNECTION:{
        if(repeatedRequest >= 3)
        {
            timerOfAction->stop();
            choiceOfAction = NOT_TO_DO;
            repeatedRequest = 0;
            emit sendNotAvaliable();
        }
        else
        {
            sendDatagram();
        }
        break;}
    case LOAD_FILE:{
        if(repeatedRequest >= 7)
        {
            /* some code */
        }
        else
        {
            sendDatagram();
        }
        break;}
    case GIVE_INFO:{
        if(repeatedRequest >= 2)
        {
            /* some code */
        }
        else
        {
            sendDatagram();
        }
        break;}
    }
}

void CustomSocket::sendDatagram()
{
    datagram.clear();
    quint8 zeroByte = 0x00;

    switch(choiceOfAction)
    {
    case NOT_TO_DO:{
        break;}
    case CHECK_CONNECTION:{
        repeatedRequest++;
        datagram.append(0x17);
        datagram.append(zeroByte);

        datagram.append(0x01);
        datagram.append(0xFD);

        datagram.append(0x1A);

        socket->write(datagram);
        break;}
    case LOAD_FILE:{
        repeatedRequest++;
        datagram.append(0x15);
        datagram.append(0x11);

        datagram.append(0x04);
        datagram.append(0xDA);

        datagram.append(((quint8*)&savedPart)[0]);
        datagram.append(((quint8*)&savedPart)[1]);
        datagram.append(((quint8*)&savedPart)[2]);
        datagram.append(((quint8*)&savedPart)[3]);

        socket->write(datagram);
        break;}
    case GIVE_INFO:{
        repeatedRequest++;
        datagram.append(0x04);
        datagram.append(0xFF);

        datagram.append(0x01);
        datagram.append(zeroByte);

        datagram.append(((quint8*)&currentPack)[0]);
        datagram.append(((quint8*)&currentPack)[1]);
        datagram.append(((quint8*)&currentPack)[2]);
        datagram.append(((quint8*)&currentPack)[3]);
        socket->write(datagram);
        break;}
    }

    if(choiceOfAction != NOT_TO_DO)
        timerOfAction->start();
}

void CustomSocket::takeDatagram()
{
    datagram.clear();
    timerOfAction->stop();
    datagram = socket->read(socket->pendingDatagramSize());

    switch(choiceOfAction)
    {
    case NOT_TO_DO:{
        break;}
    case CHECK_CONNECTION:{
        if(!datagram.isEmpty() && datagram.at(4) == 0x01)
        {
            requestSize = 0;
            emit sendConnectionAccept();

            /* some code */
            sendDatagram();
        }
        else
        {
            requestSize++;
            timerOfAction->start(timeInterval);
        }
        break;}
    case LOAD_FILE:{
        repeatedRequest = 0;
        quint16 sizePack;
        savedBlocs++;
        sizePack = (datagram.at(2)*1) + (datagram.at(3)*256);
        if(sizePack != 6)
        {
            if(savedBlocs == 1)
            {
                /* some code */
            }
            else
            {
                savedFile.write(datagram.mid(10));
            }
            savedPart++;
            sendDatagram();
        }
        else
        {
            if(savedBlocs == 1)
            {
                /* some code */
                emit sendErrorLoadingFile();
            }
            else
            {
                savedFile.flush();
                savedFile.close();
                /* some code */
                emit sendLoadingFinished();
            }
        }
        break;}
    case GIVE_INFO:{
        if(datagram.size() == 155)
        {
            repeatedRequest = 0;
            quint32 numberJ = ((quint8)datagram.at(9)*1 + (quint8)datagram.at(10)*256
                                     +(quint8)datagram.at(11)*256*256 + (quint8)datagram.at(12)*256*256*256);

            if(numberJ == 0)
            {
                choiceOfAction = NOT_TO_DO;
            }
            else
            {
                logFile->seek(logFile->size() - journalProcess->getSizePack());
                QByteArray readBa = logFile->read(journalProcess->getSizePack());
                numberProject = ((quint8)readBa.at(126)*1)+((quint8)readBa.at(127)*256)
                        +((quint8)readBa.at(128)*256*256)+((quint8)readBa.at(129)*256*256*256);

                numberProject++;

                /* some code */

                datagram.append(((quint8*)&numberProject)[0]);
                datagram.append(((quint8*)&numberProject)[1]);
                datagram.append(((quint8*)&numberProject)[2]);
                datagram.append(((quint8*)&numberProject)[3]);

                /* some code */

                emit sendAddNewJournalRecords();

                quint16 component = ((quint8)datagram.at(19)*1) + ((quint8)datagram.at(20)*256);

                datagram = reCreatePack(datagram, component);
                logFile->write(datagram.mid(9));
                logFile->flush();

               /* some code */

                quint8 bit01;
                quint8 bit26;
                bit01 = (quint8)datagram.mid(9).at(52) & 0x03;
                bit26 = (quint8)datagram.mid(9).at(52) >> 2 &0x1F;
                if(bit01 == 2 && bit26 == 13 && objectsMap->contains(component))
                {
                    /* some code */
                }
            }
        }
        else
        {
            timerOfAction->start();
        }
        break;}
    }
}


void CustomSocket::receiveConnectToCurrentHost(QString ip)
{
    socket->abort();
    socket->connectToHost(ip, 1010);
}

void CustomSocket::receiveSocketStartTimer()
{
    if(socket->pendingDatagramSize() > 0)
        socket->read(socket->pendingDatagramSize());

    /*some code */

    timerOfAction->start();
}

void CustomSocket::receiveSocketStopTimer()
{
    choiceOfAction = NOT_TO_DO;
    timerOfAction->stop();
}

void CustomSocket::receiveNeedCloseSocket()
{
    choiceOfAction = NOT_TO_DO;
    timerOfAction->stop();

    /* some code */

    socket->disconnectFromHost();
    emit sendSocketFinished();
}




