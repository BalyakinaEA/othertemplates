#include "CustomDataBase.h"

CustomDataBase::CustomDataBase(QObject *parent)
{   
    /* some code */
}

CustomDataBase::~CustomDataBase()
{

}

bool CustomDataBase::openDataBase()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setHostName(DATABASE_HOSTNAME);
    db.setDatabaseName(homePath + "/" + + DATABASE_NAME);
    if(db.open())
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool CustomDataBase::connectToDataBase()
{
    if(!QFile(homePath + "/" + DATABASE_NAME).exists())
    {
        return this->restoreDataBase();
    }
    else
    {
        return this->openDataBase();
    }
}

bool CustomDataBase::restoreDataBase()
{   
    if(this->openDataBase())
    {        
        return(this->createTable()) ? true : false;
    }
    else // Could not restore database
    {       
        return false;
    }
    return false;
}

void CustomDataBase::closeDataBase()
{
    db.close();
}

bool CustomDataBase::checkExistObjectTable(QString uidObject)
{
    uidObject.append("]");
    uidObject.prepend("[");

    QSqlQuery queryMain;
    QString uidSection = sectionsMap.value("someValue");

    if(queryMain.exec("SELECT * FROM " TABLE " WHERE " + uidSection + " = '" + uidObject + "'"))
    {     
        queryMain.first();queryMain.next();
        return queryMain.first();
    }
    else
    {
        return false;
    }
}

bool CustomDataBase::createTable()
{
    QSqlQuery query;
    if(!query.exec( "CREATE TABLE " TABLE " ("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    /* some code */
                    " )"
                    ))
    {       
        return false;
    }
    else
    {
        return true;
    }
    return false;
}

bool CustomDataBase::createNewJournalTable(QString nameTable)
{
    bool result = true;
    QList<QString> newList;
    for(int ii = 0; ii < sectionsMap.size(); ii++)
    {
        if(sectionsMap.value(ii) != TABLE_UID_OBJECT)
        {
            QString newNameTable =  "[" + nameTable + "_" + sectionsMap.value(ii) + "]";           

            QSqlQuery query;
            if(!query.exec("CREATE TABLE " +newNameTable+ " ("
                           "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                           "someValue"  " VARCHAR(255)    NOT NULL,"
                           "someValue"  " VARCHAR(255)    NOT NULL,"
                           "someValue"  " VARCHAR(255)    NOT NULL,"
                           "someValue"  " VARCHAR(255)    NOT NULL,"
                           "someValue"  " VARCHAR(255)    NOT NULL,"
                           "someValue"  " VARCHAR(255)    NOT NULL"
                           " )"
                           ))
            {              
                result = result & false;
            }
            else
            {
                newList.append(newNameTable);
                result = result & true;
            }
        }
    }

    if(result)
    {
        updateDataToMainTable(newList);
    }

    return result;
}

bool CustomDataBase::inserIntoTable(quint8 section, QString nameTable, const QVariantList &data)
{
    QString newNameTable = nameTable + "_" + sectionsMap.value(section);
    newNameTable.append("]");
    newNameTable.prepend("[");

    QSqlQuery query;
    query.prepare("INSERT INTO " + newNameTable+ " ( "
                  "someValue"
                                    "VALUES (:s1, :s2, :s3, :s4)");
    query.bindValue(":s1", data[0].toString());
    query.bindValue(":s2", data[1].toString());
    query.bindValue(":s3", data[2].toString());
    query.bindValue(":s4", data[3].toString());

    if(!query.exec())
    {       
        return false;
    }
    else
    {
        return true;
    }
    return false;
}

void CustomDataBase::deleteFromTable(quint8 section, QString nameTable, QString uidObject)
{
    QString newNameTable = nameTable + "_" + sectionsMap.value(section);

    QSqlQuery query;
    QString commandQuery = "DELETE FROM " + newNameTable + " WHERE Uid = " + "'" + uidObject + "'";

    query.prepare(commandQuery);
    query.exec();
}

bool CustomDataBase::updateDataToMainTable(QList<QString> newList)
{   
    QStringList list = newList.first().split("_");
    QString uidObject = list.first();

    QSqlQuery query;
    {        
        query.prepare("INSERT INTO " TABLE"("
                      "someValue"
                      "VALUES (:s0, :s1, :s2, :s3, :s4, :s5, :s6, :s7)");

        query.bindValue(":s0",  uidObject);
        query.bindValue(":s1",  newList.at(0));
        query.bindValue(":s2",  newList.at(1));
        query.bindValue(":s3",  newList.at(2));
        query.bindValue(":s4",  newList.at(3));
        query.bindValue(":s5",  newList.at(4));
        query.bindValue(":s6",  newList.at(5));
        query.bindValue(":s7",  newList.at(6));        
    }

    if(!query.exec())
    {       
        return false;
    }
    else
    {
        return true;
    }
    return false;
}

void CustomDataBase::selectInfoFromTable(QString uidObject)
{
    QString uidSection = sectionsMap.value("someValue");
    QSqlQuery queryMain;

    if(queryMain.exec("SELECT * FROM " TABLE " WHERE " + uidSection + " = '" + uidObject + "'"))
    {
        queryMain.next();        
        int index = 1;
        for(int ii = 1; ii < sectionsMap.size(); ii++)
        {
            QString tableName = queryMain.value(index+ii).toString();           
            QSqlQuery query;

            QStringList ll = tableName.split("_");
            QString valueStr = ll.last();         
            quint8 key = sectionsMap.key(valueStr);

            if(query.exec("SELECT " + "someValue " + " FROM " + tableName))
            {
                while(query.next())
                {
                    QVariantList listQueryValues;
                    for(int jj = 0; jj < 5; jj++)
                    {
                        listQueryValues.append(query.value(jj));
                    }
                    emit sendDataFromDB(key, listQueryValues);
                }
            }
            else
            {
                //Error
                /* some code */
            }
        }
    }
}

void CustomDataBase::clearAllDataTable(QString id)
{
    for(int ii = 1;  ii < sectionsMap.size(); ii++)
    {
        QString newNameTable =  "[" + id + "_" + sectionsMap.value(ii) + "]";
        QSqlQuery query;
        QString command = "DELETE FROM " + newNameTable;
        query.exec(command);
    }
}

void CustomDataBase::receiveNeedCloseDB()
{   
    db.removeDatabase(db.databaseName());
    db.close();
    emit sendDBFinished();
}

void CustomDataBase::receiveAddDataToDBSection(quint8 section, QString time, ObjectK *currentObject)
{
    /* some code */

    QVariantList data;
    data.append(time);
    data.append(number);
    data.append(name);
    data.append(type);    
    data.append(uid);

    bool result = /* some code - chek Exist Uid In Table */;

    if(!result)
        inserIntoTable(section, nameTable , data);
}

void CustomDataBase::receiveDeleteDataFromDBSection(quint8 section, QString uidObject)
{
    QString idObject =  settings->value("somePath").toString();
    deleteFromTable(section, idObject, uidObject);
}


