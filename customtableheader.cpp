#include "CustomTableHeader.h"

CustomTableHeader::CustomTableHeader(Qt::Orientation orientation, QWidget *parent) : QHeaderView(orientation, parent)
{
    this->setSortIndicatorShown(false);
    setSortIndicator(-1, Qt::AscendingOrder);
}

void CustomTableHeader::mouseReleaseEvent(QMouseEvent *e)
{
    const int index = logicalIndexAt(e->pos());
    if(e->button() == Qt::LeftButton)
    {
        setSortingOrder(index);
    }
    else
    {
        QRect rect;
        if(this->orientation() == Qt::Horizontal)
        {
            rect.setLeft(this->sectionPosition(index));
            rect.setWidth(this->sectionSize(index)/3);
            rect.setTop(0);
            rect.setHeight(this->height());
        }
        else
        {
            rect.setTop(this->sectionPosition(index));
            rect.setHeight(this->sectionSize(index));
            rect.setLeft(0);
            rect.setWidth(this->width());
        }

        switch(index)
        {
        case 0:
            /* some code */
            break;
        case 3://factory number
            factoryEditor = new QLineEdit(this->viewport());
            factoryEditor->move(rect.topLeft());
            factoryEditor->resize(rect.size());
            factoryEditor->setMinimumWidth(factoryEditor->width()*3);
            factoryEditor->setFrame(false);
            factoryEditor->installEventFilter(this);
            factoryEditor->setFocus();
            factoryEditor->setPlaceholderText("Найти...");
            connect(factoryEditor, &QLineEdit::returnPressed,
                    this, &CustomTableHeader::getFactoryNumber);
            factoryEditor->show();

            break;
        }
    }
    QHeaderView::mouseReleaseEvent(e);
}

void CustomTableHeader::mousePressEvent(QMouseEvent *e)
{
    QHeaderView::mousePressEvent(e);
}

bool CustomTableHeader::eventFilter(QObject *o, QEvent *e)
{
    if (o == typeEditor && e->type() == QEvent::FocusOut)
    {
        getNameToSearch();
        return true;
    }

    if (o == factoryEditor && e->type() == QEvent::FocusOut)
    {
        getFactoryNumber();
        return true;
    }
    return false;
}

void CustomTableHeader::createFilterButton()
{
    const int index = logicalIndexAt(1);
    QRect rect;
    if (this->orientation() == Qt::Horizontal)
    {
        rect.setLeft(this->sectionPosition(index));
        rect.setWidth(this->sectionSize(index));
        rect.setTop(0);
        rect.setHeight(this->height());

    }else {
        rect.setTop(this->sectionPosition(index));
        rect.setHeight(this->sectionSize(index));
        rect.setLeft(0);
        rect.setWidth(this->width());
    }

    filterBtn = new QPushButton(this->viewport());
    filterBtn->setIcon(QIcon(":/icons/down-button.png"));
    filterBtn->move(rect.topLeft());
    filterBtn->resize(rect.size());
    filterBtn->setMaximumWidth(20);
    filterBtn->installEventFilter(this);
    filterBtn->setFocus();
    filterBtn->show();
    connect(filterBtn, &QPushButton::clicked, this, [this]{sendPressOpenFilterBtn(this->mapToGlobal(filterBtn->pos()));});
}

void CustomTableHeader::getNameToSearch()
{
    QString nameToSearch = typeEditor->text();
    typeEditor->deleteLater();
    typeEditor = 0;

    if(!nameToSearch.isEmpty())
        emit sendSearchDeviceName(nameToSearch);
}

void CustomTableHeader::getFactoryNumber()
{
    QString factoryToSearch = factoryEditor->text();
    factoryEditor->deleteLater();
    factoryEditor = 0;

    if(!factoryToSearch.isEmpty())
        emit sendSearchFactoryNumber(factoryToSearch);
}

void CustomTableHeader::setSortingOrder(int column)
{  
    QTableView *tableView = qobject_cast <QTableView*>(this->parent());
    if(tableView != 0 && tableView->isSortingEnabled())
    {
        if(column != this->sortIndicatorSection())
        {
            tableView->sortByColumn(column, Qt::AscendingOrder);
            order = AscendingOrder;
            this->setSortIndicatorShown(true);
        }
        else
        {
            switch (order)
            {
            case AscendingOrder:{
                tableView->sortByColumn(column, Qt::DescendingOrder);
                order = DescendingOrder;
                this->setSortIndicatorShown(true);
                break;}
            case DescendingOrder:{
                emit sendResetSorting();
                order = Unsorted;
                setSortIndicator(-1, Qt::AscendingOrder);
                break;}
            case Unsorted:{
                tableView->sortByColumn(column, Qt::AscendingOrder);
                order = AscendingOrder;
                this->setSortIndicatorShown(true);
                break;}
            default:
                break;
            }
        }
    }
}


