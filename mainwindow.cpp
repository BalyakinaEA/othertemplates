#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    /* some code */

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::creatThreds()
{
    threadSocket = new QThread();

    socketObject = new CustomSocket();
    connect(threadSocket, &QThread::started, socketObject, &CustomSocket::createSWData);
    connect(threadSocket, &QThread::finished, socketObject, &CustomSocket::deleteLater);

    connect(socketObject, &CustomSocket::sendSocketFinished, this, &MainWindow::receiveSocketFinished);

    /* some code */

    socketObject->moveToThread(threadSocket);
    threadSocket->start();
}

void MainWindow::checkSavedFile()
{
    QFile savedFile;
    savedFile.setFileName("somePath");
    savedFile.open(QIODevice::ReadOnly);

    QDataStream in(&savedFile);
    QByteArray buffer;
    buffer.resize(savedFile.size());
    in.readRawData(buffer.data(),buffer.length());
    savedFile.close();

    QByteArray hash;
    QCryptographicHash crypto(QCryptographicHash::Md5);
    crypto.addData(buffer.data(), buffer.length());
    hash = crypto.result();

    QByteArray loadingHash = settings->value(settingsProcess->getSavedMD5Directory()).toByteArray();

    if(loadingHash == hash)
    {
        /* some code */
    }
    else
    {
        /* some code */
    }
}

void MainWindow::unzipSavedFile()
{
    bool izUnzip = true;

    QDir extractDir("./Configuration");

    QuaZip zip("somePath");
    if(zip.open(QuaZip::mdUnzip))
    {
        for (bool more = zip.goToFirstFile(); more; more = zip.goToNextFile())
        {
            QString filePath = zip.getCurrentFileName();
            QuaZipFile zFile(zip.getZipName(), filePath);
            zFile.open( QIODevice::ReadOnly);
            if(zFile.isOpen())
            {
                QByteArray ba = zFile.readAll();
                zFile.close();

                QString newFilePath = QString(extractDir.dirName() + "/" + filePath);

                QFileInfo fi(newFilePath);
                QDir dir;
                dir.mkdir(fi.absoluteDir().path());

                QFile dstFile(newFilePath);
                dstFile.open( QIODevice::WriteOnly);
                if(dstFile.isOpen())
                {
                    dstFile.write(ba);
                    dstFile.close();
                }
                else
                {
                    izUnzip = false;
                }
            }
            else
            {
                izUnzip = false;
            }

        }
        copyPath("./Configuration", "somePath");
        extractDir.removeRecursively();
    }
    else
    {
        izUnzip = false;
    }

    if(izUnzip)
    {
        /* some code */
    }
    else
    {
        /* some code */
    }
}

void MainWindow::copyPath(const QString &oldPath, const QString &newPath)
{
    if(QFileInfo(oldPath ).isDir())
    {
        QDir().mkdir(newPath);
        foreach (const QString & entry, QDir(oldPath).entryList(QDir::AllDirs | QDir::Files | QDir::Hidden | QDir::NoDotAndDotDot ))
            copyPath(oldPath + QDir::separator() + entry, newPath + QDir::separator() + entry);
    }
    else
    {
        QFile::copy(oldPath, newPath);
    }
}

void MainWindow::receiveSocketFinished()
{
    threadSocket->quit();
    threadSocket->wait(500);
}
