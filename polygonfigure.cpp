#include "polygonfigure.h"

PolygonFigure::PolygonFigure(QList<QPointF> point)
{
    setAcceptHoverEvents(true);
    setOpacity(0.5);
    setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemSendsGeometryChanges);


    QPolygonF poly;
    for(int ii = 0; ii < point.size(); ii++)
    {
        poly << point.at(ii);
    }
    setPolygon(poly);
}

void PolygonFigure::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    update();
    QGraphicsItem::hoverEnterEvent(event);
    event->accept();
}

void PolygonFigure::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QBrush brush(Qt::gray);
    QPen pen;

    pen = QPen(Qt::black, 2);
    painter->setBrush(brush);
    painter->setPen(pen);
    painter->drawPolygon(this->polygon());

    if(this->isSelected())
    {
        pen = QPen(Qt::red, 5);
        painter->setOpacity(1);
        painter->setBrush(QBrush());
        painter->setPen(pen);
        painter->drawPolygon(this->polygon());
    }
    this->scene()->update();
}





