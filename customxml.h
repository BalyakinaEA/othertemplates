#ifndef CustomXML_H
#define CustomXML_H

#include <QObject>
#include <QString>
#include <QMap>
#include <QFile>
#include <QXmlStreamReader>
#include <QStandardItem>
#include <QDebug>
#include <QMessageBox>
#include <QPointF>
#include <QHash>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QFont>
#include <QFontDatabase>

#include "typedevice.h"

class CustomXML : public QObject
{
    Q_OBJECT

public:
    CustomXML(QString fileName);
    void setFileNameParseXML(QString fileName);

    QHash<unsigned int, TypeDevice *> parseTypeListMain();
    void parseType(TypeDevice *typeDevices);
    void parseParametr(TypeDevice *typeDevices);
    void parseButtons(TypeDevice *typeDevices);

private:
    QFile fileXml;
    QXmlStreamReader readerXml;   
    int openedXmlVersion;
};

#endif // CustomXML_H
